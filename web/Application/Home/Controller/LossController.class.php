<?php

namespace Home\Controller;

use Think\Controller;
use Home\Service\UserService;
use Home\Common\FIdConst;
use Home\Service\LOBillService;

/**
 * 报损报溢Controller
 *
 * @author 李静波
 *        
 */
class LossController extends PSIBaseController {

	/**
	 * 报损报溢 - 主页面
	 */
	public function index() {
		$us = new UserService();
		if ($us->hasPermission(FIdConst::LOSS_OR_OVER_MANAGEMNET)) {
			$this->initVar();
			
			$this->assign("title", "报损报溢");
			
			$this->display();
		} else {
			$this->gotoLoginPage("/Home/Loss/index");
		}
	}

	public function loBillInfo() {
		if (IS_POST) {
			$params = array(
					"id" => I("post.id")
			);
			
			$ls = new LOBillService();
			
			$this->ajaxReturn($ls->loBillInfo($params));
		}
	}

	/**
	 * 新增或保存损溢单
	 */
	public function editLOBill() {
		if (IS_POST) {
			$params = array(
					"jsonStr" => I("post.jsonStr")
			);
			
			$ls = new LOBillService();
			
			$this->ajaxReturn($ls->editLOBill($params));
		}
	}

	/**
	 * 主表
	 */
	public function lobillList() {
		if (IS_POST) {
			$params = array(
					"billStatus" => I("post.billStatus"),
					"ref" => I("post.ref"),
					"fromDT" => I("post.fromDT"),
					"toDT" => I("post.toDT"),
					"warehouseId" => I("post.warehouseId"),
					"start" => I("post.start"),
					"limit" => I("post.limit")
			);
			
			$ls = new LOBillService();
			
			$this->ajaxReturn($ls->lobillList($params));
		}
	}

	/**
	 * 明细记录
	 */
	public function loBillDetailList() {
		if (IS_POST) {
			$params = array(
					"id" => I("post.id")
			);
			
			$ls = new LOBillService();
			
			$this->ajaxReturn($ls->loBillDetailList($params));
		}
	}

	/**
	 * 删除损溢单
	 */
	public function deleteLOBill() {
		if (IS_POST) {
			$params = array(
					"id" => I("post.id")
			);
			
			$ls = new LOBillService();
			
			$this->ajaxReturn($ls->deleteLOBill($params));
		}
	}

	/**
	 * 审核损溢单
	 */
	public function commitLOBill() {
		if (IS_POST) {
			$params = array(
					"id" => I("post.id")
			);
			
			$ls = new LOBillService();
			
			$this->ajaxReturn($ls->commitLOBill($params));
		}
	}

	/**
	 * 取消审核损溢单
	 */
	public function cancelCommitLOBill() {
		if (IS_POST) {
			$params = array(
					"id" => I("post.id")
			);
			
			$ls = new LOBillService();
			
			$this->ajaxReturn($ls->cancelCommitLOBill($params));
		}
	}
}