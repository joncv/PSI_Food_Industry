<?php

namespace Home\DAO;

use Home\Service\IdGenService;

/**
 * 客户资料 DAO
 *
 * @author 李静波
 */
class CustomerDAO extends PSIBaseDAO {
	private $LOG_CATEGORY = "客户关系-客户资料";

	/**
	 * 获得所有的价格体系中的价格
	 */
	public function priceSystemList($params) {
		$id = $params["id"];
		
		$db = M();
		$sql = "select id, name from t_price_system";
		
		$data = $db->query($sql);
		
		$result = array();
		$result[0]["id"] = "-1";
		$result[0]["name"] = "[无]";
		foreach ( $data as $i => $v ) {
			$result[$i + 1]["id"] = $v["id"];
			$result[$i + 1]["name"] = $v["name"];
		}
		
		$psId = null;
		if ($id) {
			$sql = "select ps_id from t_customer_category where id = '%s' ";
			$data = $db->query($sql, $id);
			if ($data) {
				$psId = $data[0]["ps_id"];
			}
		}
		
		return array(
				"psId" => $psId,
				"priceList" => $result
		);
	}

	public function editProvince($params) {
		$id = $params["id"];
		$name = $params["name"];
		
		$db = M();
		
		if ($id) {
			// 编辑
			$sql = "select count(*) as cnt from t_province 
					where name = '%s' and id <> '%s' ";
			$data = $db->query($sql, $name, $id);
			$cnt = $data[0]["cnt"];
			if ($cnt > 0) {
				return $this->bad("省[$name]已经存在");
			}
			
			$sql = "update t_province
					set name = '%s'
					where id = '%s' ";
			$db->execute($sql, $name, $id);
		} else {
			// 新增
			$sql = "select count(*) as cnt from t_province where name = '%s' ";
			$data = $db->query($sql, $name);
			$cnt = $data[0]["cnt"];
			if ($cnt > 0) {
				return $this->bad("省[$name]已经存在");
			}
			$idGen = new IdGenService();
			$id = $idGen->newId($db);
			$sql = "insert into t_province (id, name)
					values ('%s', '%s')";
			$db->execute($sql, $id, $name);
		}
		
		return $this->ok($id);
	}

	public function provinceList() {
		$db = M();
		$sql = "select id, name from t_province
				order by name";
		return $db->query($sql);
	}

	public function deleteProvince($params) {
		$id = $params["id"];
		
		$db = M();
		$sql = "select count(*) as cnt from t_city where province_id = '%s' ";
		$data = $db->query($sql, $id);
		$cnt = $data[0]["cnt"];
		if ($cnt > 0) {
			return $this->bad("还有市用到了本省，不能删除");
		}
		
		$sql = "delete from t_province where id = '%s' ";
		$db->execute($sql, $id);
		
		return $this->ok();
	}

	public function editCity($params) {
		$id = $params["id"];
		$name = $params["name"];
		$provinceId = $params["provinceId"];
		
		$db = M();
		$sql = "select name from t_province where id = '%s' ";
		$data = $db->query($sql, $provinceId);
		if (! $data) {
			return $this->bad("没有选择省份");
		}
		
		if ($id) {
			// 编辑
			$sql = "select count(*) as cnt from t_city
					where name = '%s' and province_id = '%s' 
						and id <> '%s' ";
			$data = $db->execute($sql, $name, $provinceId, $id);
			$cnt = $data[0]["cnt"];
			if ($cnt > 0) {
				return $db->bad("市[$name]已经存在");
			}
			
			$sql = "update t_city
					set name = '%s', province_id = '%s' 
					where id = '%s'  ";
			$db->execute($sql, $name, $provinceId, $id);
		} else {
			// 新增
			$idGen = new IdGenService();
			$id = $idGen->newId($db);
			
			$sql = "select count(*) as cnt from t_city
					where name = '%s' and province_id = '%s' ";
			$data = $db->execute($sql, $name, $provinceId);
			$cnt = $data[0]["cnt"];
			if ($cnt > 0) {
				return $db->bad("市[$name]已经存在");
			}
			
			$sql = "insert into t_city(id, name, province_id)
					values ('%s', '%s', '%s')";
			$db->execute($sql, $id, $name, $provinceId);
		}
		
		return $this->ok($id);
	}

	public function cityList($params) {
		$provinceId = $params["provinceId"];
		
		$db = M();
		$sql = "select id, name, province_id
				from t_city
				where province_id = '%s' 
				order by name";
		$data = $db->query($sql, $provinceId);
		
		$result = array();
		foreach ( $data as $i => $v ) {
			$result[$i]["id"] = $v["id"];
			$result[$i]["name"] = $v["name"];
			$result[$i]["provinceId"] = $v["province_id"];
		}
		return $result;
	}

	public function deleteCity($params) {
		$id = $params["id"];
		
		$db = M();
		$sql = "select count(*) as cnt from t_district where city_id = '%s' ";
		$data = $db->query($sql, $id);
		$cnt = $data[0]["cnt"];
		if ($cnt > 0) {
			return $this->bad("还有区县用到了本市，不能删除");
		}
		
		$sql = "delete from t_city where id = '%s' ";
		$db->execute($sql, $id);
		
		return $this->ok();
	}

	public function editDistrict($params) {
		$id = $params["id"];
		$cityId = $params["cityId"];
		$name = $params["name"];
		
		$db = M();
		$sql = "select name from t_city where id = '%s' ";
		$data = $db->query($sql, $cityId);
		if (! $data) {
			return $this->bad("市不存在");
		}
		
		if ($id) {
			// 编辑
			$sql = "select count(*) as cnt 
					from t_district 
					where name = '%s' and city_id = '%s' and id <> '%s' ";
			$data = $db->execute($sql, $name, $cityId, $id);
			$cnt = $data[0]["cnt"];
			if ($cnt > 0) {
				return $this->bad("区县[$name]已经存在");
			}
			
			$sql = "update t_district
					set name = '%s', city_id = '%s'
					where id = '%s' ";
			$db->execute($sql, $name, $cityId, $id);
		} else {
			// 新增
			
			$sql = "select count(*) as cnt 
					from t_district 
					where name = '%s' and city_id = '%s' ";
			$data = $db->execute($sql, $name, $cityId);
			$cnt = $data[0]["cnt"];
			if ($cnt > 0) {
				return $this->bad("区县[$name]已经存在");
			}
			
			$idGen = new IdGenService();
			$id = $idGen->newId($db);
			
			$sql = "insert into t_district(id, city_id, name)
					values ('%s', '%s', '%s') ";
			$db->execute($sql, $id, $cityId, $name);
		}
		
		return $this->ok($id);
	}

	public function districtList($params) {
		$cityId = $params["cityId"];
		
		$db = M();
		$sql = "select id, name
				from t_district
				where city_id = '%s'
				order by name ";
		return $db->query($sql, $cityId);
	}

	public function deleteDistrict($params) {
		$id = $params["id"];
		
		$db = M();
		$sql = "delete from t_district where id = '%s' ";
		$db->execute($sql, $id);
		
		return $this->ok();
	}

	public function genAllDistrictData() {
		$db = M();
		
		$idGen = new IdGenService();
		
		$sql = "select province, city, district
				from t_customer
				order by code";
		$data = $db->query($sql);
		foreach ( $data as $c ) {
			$province = $c["province"];
			$city = $c["city"];
			$district = $c["district"];
			
			if (! $province) {
				continue;
			}
			
			$sql = "select id from t_province where name = '%s' ";
			$d = $db->query($sql, $province);
			$provinceId = null;
			if (! $d) {
				$provinceId = $idGen->newId($db);
				$sql = "insert into t_province(id, name)
						values ('%s', '%s')";
				$db->execute($sql, $provinceId, $province);
			} else {
				$provinceId = $d[0]["id"];
			}
			
			if (! $city) {
				continue;
			}
			
			$sql = "select id from t_city where province_id = '%s' and name = '%s' ";
			$d = $db->query($sql, $provinceId, $city);
			$cityId = null;
			if (! $d) {
				$cityId = $idGen->newId($db);
				$sql = "insert into t_city (id, name, province_id)
						values ('%s', '%s', '%s')";
				$db->execute($sql, $cityId, $city, $provinceId);
			} else {
				$cityId = $d[0]["id"];
			}
			
			if (! $district) {
				continue;
			}
			
			$sql = "select id from t_district
					where city_id = '%s' and name = '%s' ";
			$d = $db->query($sql, $cityId, $district);
			if (! $d) {
				$districtId = $idGen->newId($db);
				$sql = "insert into t_district (id, name, city_id)
						values ('%s', '%s', '%s')";
				$db->execute($sql, $districtId, $district, $cityId);
			}
		}
		
		return $this->ok();
	}
}