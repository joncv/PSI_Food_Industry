/**
 * 发件人信息编辑界面
 */
Ext.define("PSI.Express.SenderEditForm", {
	extend : "Ext.window.Window",

	config : {
		parentForm : null,
		entity : null
	},

	/**
	 * 初始化组件
	 */
	initComponent : function() {
		var me = this;

		var buttons = [];

		var btn = {
			text : "保存",
			formBind : true,
			iconCls : "PSI-button-ok",
			handler : function() {
				me.onOK(false);
			},
			scope : me
		};
		buttons.push(btn);

		var btn = {
			text : "取消",
			handler : function() {
				me.close();
			},
			scope : me
		};
		buttons.push(btn);

		Ext.apply(me, {
					title : "设置发件人信息",
					modal : true,
					resizable : false,
					onEsc : Ext.emptyFn,
					width : 400,
					height : 200,
					layout : "fit",
					listeners : {
						show : {
							fn : me.onWndShow,
							scope : me
						},
						close : {
							fn : me.onWndClose,
							scope : me
						}
					},
					items : [{
								id : "editForm",
								xtype : "form",
								layout : {
									type : "table",
									columns : 1
								},
								height : "100%",
								bodyPadding : 5,
								defaultType : 'textfield',
								fieldDefaults : {
									labelWidth : 80,
									labelAlign : "right",
									labelSeparator : "",
									msgTarget : 'side',
									width : 370,
									margin : "5"
								},
								items : [{
											id : "editName",
											fieldLabel : "发件人姓名",
											allowBlank : false,
											blankText : "没有输入发件人姓名",
											beforeLabelTextTpl : PSI.Const.REQUIRED,
											name : "name",
											listeners : {
												specialkey : {
													fn : me.onEditSpecialKey,
													scope : me
												}
											}
										}, {
											id : "editTel",
											fieldLabel : "发件人电话",
											allowBlank : false,
											blankText : "没有输入发件人电话",
											beforeLabelTextTpl : PSI.Const.REQUIRED,
											name : "tel",
											listeners : {
												specialkey : {
													fn : me.onEditSpecialKey,
													scope : me
												}
											}
										}, {
											id : "editAddress",
											fieldLabel : "发件人地址",
											allowBlank : false,
											blankText : "没有输入发件人地址",
											beforeLabelTextTpl : PSI.Const.REQUIRED,
											name : "address",
											listeners : {
												specialkey : {
													fn : me.onLastEditSpecialKey,
													scope : me
												}
											}
										}],
								buttons : buttons
							}]
				});

		me.callParent(arguments);

		me.__editorList = ["editName", "editTel", "editAddress"];
	},

	/**
	 * 保存
	 */
	onOK : function() {
		var me = this;

		var f = Ext.getCmp("editForm");
		var el = f.getEl();
		el.mask(PSI.Const.SAVING);
		var sf = {
			url : PSI.Const.BASE_URL + "Home/Express/editSenderInfo",
			method : "POST",
			success : function(form, action) {
				el.unmask();

				PSI.MsgBox.tip("数据保存成功");
				me.close();
			},
			failure : function(form, action) {
				el.unmask();
				PSI.MsgBox.showInfo(action.result.msg, function() {
							Ext.getCmp("editName").focus();
						});
			}
		};
		f.submit(sf);
	},

	onEditSpecialKey : function(field, e) {
		if (e.getKey() === e.ENTER) {
			var me = this;
			var id = field.getId();
			for (var i = 0; i < me.__editorList.length; i++) {
				var editorId = me.__editorList[i];
				if (id === editorId) {
					var edit = Ext.getCmp(me.__editorList[i + 1]);
					edit.focus();
					edit.setValue(edit.getValue());
				}
			}
		}
	},

	onLastEditSpecialKey : function(field, e) {
		var me = this;

		if (e.getKey() == e.ENTER) {
			var f = Ext.getCmp("editForm");
			if (f.getForm().isValid()) {
				me.onOK();
			}
		}
	},

	onWndClose : function() {
		var me = this;
	},

	onWndShow : function() {
		var edit = Ext.getCmp("editName");
		edit.focus();
		edit.setValue(edit.getValue());

		var el = Ext.getBody();
		el.mask(PSI.Const.LOADING);
		var r = {
			url : PSI.Const.BASE_URL + "Home/Express/getSenderInfo",
			params : {},
			method : "POST",
			callback : function(options, success, response) {
				el.unmask();

				if (!success) {
					return;
				}

				var data = Ext.JSON.decode(response.responseText);

				Ext.getCmp("editName").setValue(data.name);
				Ext.getCmp("editTel").setValue(data.tel);
				Ext.getCmp("editAddress").setValue(data.address);
			}
		};

		Ext.Ajax.request(r);
	}
});