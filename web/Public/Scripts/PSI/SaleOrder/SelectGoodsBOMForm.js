/**
 * 选择商品组合
 */
Ext.define("PSI.SaleOrder.SelectGoodsBOMForm", {
	extend : "Ext.window.Window",
	config : {
		parentForm : null,
		warehouseId : null,
		customerId : null
	},
	initComponent : function() {
		var me = this;
		Ext.apply(me, {
					title : "选择商品组合",
					modal : true,
					onEsc : Ext.emptyFn,
					width : 1200,
					height : 600,
					layout : "border",
					items : [{
								region : "center",
								border : 0,
								bodyPadding : 10,
								layout : "fit",
								items : [me.getBOMGrid()]
							}, {
								region : "north",
								border : 0,
								layout : {
									type : "table",
									columns : 3
								},
								height : 140,
								bodyPadding : 10,
								items : [{
											html : "<h1>选择商品组合</h1>",
											border : 0,
											colspan : 3
										}, {
											id : "editGoodsCode",
											labelWidth : 100,
											labelAlign : "right",
											labelSeparator : "",
											fieldLabel : "商品组合",
											margin : "5, 0, 0, 0",
											xtype : "psi_goodsbomextfield",
											parentCmp : me,
											listeners : {
												specialkey : {
													fn : me.onEditCodeSpecialKey,
													scope : me
												}
											}
										}, {
											id : "editGoodsName",
											labelWidth : 100,
											labelAlign : "right",
											labelSeparator : "",
											fieldLabel : "名称",
											readOnly : true,
											margin : "5, 0, 0, 0",
											xtype : "textfield"
										}, {
											id : "editGoodsSpec",
											labelWidth : 100,
											labelAlign : "right",
											labelSeparator : "",
											fieldLabel : "规格型号",
											readOnly : true,
											margin : "5, 0, 0, 0",
											xtype : "textfield"
										}, {
											id : "editGoodsCount",
											labelWidth : 100,
											labelAlign : "right",
											labelSeparator : "",
											fieldLabel : "组合销售套数",
											margin : "5, 0, 0, 0",
											xtype : "numberfield",
											value : 1,
											allowDecimals : false,
											hideTrigger : true,
											listeners : {
												specialkey : {
													fn : me.onEditCountSpecialKey,
													scope : me
												}
											}
										}, {
											xtype : "button",
											text : "查询",
											iconCls : "PSI-button-refresh",
											margin : "5 0 0 5",
											width : 80,
											handler : me.onQuery,
											scope : me
										}]
							}],
					listeners : {
						show : {
							fn : me.onWndShow,
							scope : me
						}
					},
					buttons : [{
								text : "确定",
								iconCls : "PSI-button-ok",
								formBind : true,
								handler : me.onOK,
								scope : me
							}, {
								text : "取消",
								handler : function() {
									me.close();
								},
								scope : me
							}]
				});

		me.callParent(arguments);
	},

	onWndShow : function() {
		var me = this;
		Ext.getCmp("editGoodsCode").focus();
	},

	onOK : function() {
		var me = this;

		var store = me.getBOMGrid().getStore();
		if (store.getCount() == 0) {
			PSI.MsgBox.showInfo("没有查询到组合数据");
			return;
		}

		me.getParentForm().insertGoodsBOM(store);
		me.close();
	},

	getBOMGrid : function() {
		var me = this;

		if (me.__bomGrid) {
			return me.__bomGrid;
		}

		var modelName = "PSISOBill_BOMSelectForm";
		Ext.define(modelName, {
					extend : "Ext.data.Model",
					fields : ["id", "goodsId", "code", "name", "spec",
							"qcBeginDT", "expiration", "qcEndDT", "invCount",
							"goodsCount", "bomCount", "unitName", "price"]
				});
		var store = Ext.create("Ext.data.Store", {
					autoLoad : false,
					model : modelName,
					data : []
				});

		me.__bomGrid = Ext.create("Ext.grid.Panel", {
					columnLines : true,
					columns : [{
								header : "商品编码",
								dataIndex : "code",
								menuDisabled : true,
								sortable : false
							}, {
								header : "商品名称",
								dataIndex : "name",
								menuDisabled : true,
								sortable : false,
								width : 150
							}, {
								header : "规格型号",
								dataIndex : "spec",
								menuDisabled : true,
								sortable : false,
								width : 150
							}, {
								header : "生产日期",
								dataIndex : "qcBeginDT",
								menuDisabled : true,
								sortable : false
							}, {
								header : "保质期(天)",
								dataIndex : "expiration",
								menuDisabled : true,
								sortable : false
							}, {
								header : "到期日期",
								dataIndex : "qcEndDT",
								menuDisabled : true,
								sortable : false
							}, {
								header : "当前库存",
								dataIndex : "invCount",
								menuDisabled : true,
								sortable : false,
								xtype : "numbercolumn",
								align : "right",
								format : "#"
							}, {
								header : "销售数量",
								dataIndex : "goodsCount",
								menuDisabled : true,
								sortable : false,
								xtype : "numbercolumn",
								align : "right",
								format : "#"
							}, {
								header : "单位",
								dataIndex : "unitName",
								menuDisabled : true,
								sortable : false,
								width : 80
							}, {
								header : "销售价",
								dataIndex : "price",
								menuDisabled : true,
								sortable : false,
								xtype : "numbercolumn",
								align : "right",
								format : "#.##"
							}],
					listeners : {
						itemdblclick : {
							fn : me.onOK,
							scope : me
						}
					},
					store : store
				});

		return me.__bomGrid;
	},

	__setGoodsInfo : function(data) {
		var me = this;

		Ext.getCmp("editGoodsName").setValue(data.name);
		Ext.getCmp("editGoodsSpec").setValue(data.spec);
	},

	onEditCodeSpecialKey : function(field, e) {
		if (e.getKey() === e.ENTER) {
			var me = this;
			var edit = Ext.getCmp("editGoodsCount");
			edit.focus();
			edit.setValue(edit.getValue());
		}
	},

	onEditCountSpecialKey : function(field, e) {
		if (e.getKey() === e.ENTER) {
			var me = this;

			me.onQuery();
		}
	},

	onQuery : function() {
		var me = this;

		var goodsId = Ext.getCmp("editGoodsCode").getIdValue();
		if (!goodsId) {
			PSI.MsgBox.showInfo("没有选择商品组合");
			return;
		}

		var grid = me.getBOMGrid();
		var el = grid.getEl() || Ext.getBody();
		el.mask(PSI.Const.LOADING);

		var r = {
			url : PSI.Const.BASE_URL + "Home/Sale/goodsBOMList",
			params : {
				id : goodsId,
				bomCount : Ext.getCmp("editGoodsCount").getValue(),
				warehouseId : me.getWarehouseId(),
				customerId : me.getCustomerId()
			},
			method : "POST",
			callback : function(options, success, response) {
				var store = grid.getStore();

				store.removeAll();

				if (success) {
					var data = Ext.JSON.decode(response.responseText);
					store.add(data.dataList);
				} else {
					PSI.MsgBox.showInfo("网络错误");
				}

				el.unmask();
			}
		};
		Ext.Ajax.request(r);
	}
});