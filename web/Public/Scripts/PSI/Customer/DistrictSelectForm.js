/**
 * 选择地区界面
 */
Ext.define("PSI.Customer.DistrictSelectForm", {
			extend : "Ext.window.Window",
			config : {
				parentForm : null
			},

			initComponent : function() {
				var me = this;

				Ext.apply(me, {
							title : "选择地区",
							modal : true,
							resizable : false,
							onEsc : Ext.emptyFn,
							width : 900,
							height : 400,
							layout : "border",
							tbar : [{
										text : "从现有客户资料中提取",
										iconCls : "PSI-button-add-detail",
										handler : me.onGenFromCustomerData,
										scope : me
									}, "-", {
										text : "新增省份",
										iconCls : "PSI-button-add",
										handler : me.onAddProvince,
										scope : me
									}, {
										text : "编辑省份",
										iconCls : "PSI-button-edit",
										handler : me.onEditProvince,
										scope : me
									}, {
										text : "删除省份",
										iconCls : "PSI-button-delete",
										handler : me.onDeleteProvince,
										scope : me
									}, "-", {
										text : "新增市",
										iconCls : "PSI-button-add",
										handler : me.onAddCity,
										scope : me
									}, {
										text : "编辑市",
										iconCls : "PSI-button-edit",
										handler : me.onEditCity,
										scope : me
									}, {
										text : "删除市",
										iconCls : "PSI-button-delete",
										handler : me.onDeleteCity,
										scope : me
									}, "-", {
										text : "新增区县",
										iconCls : "PSI-button-add",
										handler : me.onAddDistrict,
										scope : me
									}, {
										text : "编辑区县",
										iconCls : "PSI-button-edit",
										handler : me.onEditDistrict,
										scope : me
									}, {
										text : "删除区县",
										iconCls : "PSI-button-delete",
										handler : me.onDeleteDistrict,
										scope : me
									}],
							items : [{
										region : "west",
										layout : "fit",
										width : 300,
										items : [me.getProvinceGrid()]
									}, {
										region : "center",
										layout : "border",
										border : 0,
										items : [{
													region : "west",
													layout : "fit",
													width : 300,
													items : [me.getCityGrid()]
												}, {
													region : "center",
													layout : "fit",
													items : [me
															.getDistrictGrid()]
												}]
									}],
							listeners : {
								show : {
									fn : me.onWndShow,
									scope : me
								},
								close : {
									fn : me.onWndClose,
									scope : me
								}
							},
							buttons : [{
										text : "确定",
										formBind : true,
										iconCls : "PSI-button-ok",
										handler : function() {
											me.onOK();
										},
										scope : me
									}, {
										text : "取消",
										handler : function() {
											me.close();
										},
										scope : me
									}]
						});

				me.callParent(arguments);
			},

			onWndShow : function() {
				var me = this;
				me.freshProvinceGrid();
			},

			onWndClose : function() {
				var me = this;
			},

			onOK : function() {
				var me = this;

				var item = me.getProvinceGrid().getSelectionModel()
						.getSelection();
				if (item == null || item.length != 1) {
					PSI.MsgBox.showInfo("没有选择省");
					return;
				}
				var province = item[0];

				var item = me.getCityGrid().getSelectionModel().getSelection();
				if (item == null || item.length != 1) {
					PSI.MsgBox.showInfo("没有选择市");
					return;
				}

				var city = item[0];

				var item = me.getDistrictGrid().getSelectionModel()
						.getSelection();
				if (item == null || item.length != 1) {
					PSI.MsgBox.showInfo("没有选择区县");
					return;
				}

				var district = item[0];

				me.close();

				var form = me.getParentForm();
				form.__setDistrictInfo(province, city, district);
			},

			getProvinceGrid : function() {
				var me = this;
				if (me.__provinceGrid) {
					return me.__provinceGrid;
				}

				var modelName = "PSIProvince";
				Ext.define(modelName, {
							extend : "Ext.data.Model",
							fields : ["id", "name"]
						});

				me.__provinceGrid = Ext.create("Ext.grid.Panel", {
							border : 0,
							viewConfig : {
								enableTextSelection : true
							},
							columnLines : true,
							columns : [{
										xtype : "rownumberer"
									}, {
										header : "省",
										dataIndex : "name",
										menuDisabled : true,
										sortable : false,
										flex : 1
									}],
							store : Ext.create("Ext.data.Store", {
										model : modelName,
										autoLoad : false,
										data : []
									}),
							listeners : {
								select : {
									fn : me.onProvinceGridSelect,
									scope : me
								}
							}
						});

				return me.__provinceGrid;
			},

			getCityGrid : function() {
				var me = this;
				if (me.__cityGrid) {
					return me.__cityGrid;
				}

				var modelName = "PSICity";
				Ext.define(modelName, {
							extend : "Ext.data.Model",
							fields : ["id", "name", "provinceId"]
						});

				me.__cityGrid = Ext.create("Ext.grid.Panel", {
							border : 0,
							viewConfig : {
								enableTextSelection : true
							},
							columnLines : true,
							columns : [{
										xtype : "rownumberer"
									}, {
										header : "市",
										dataIndex : "name",
										menuDisabled : true,
										sortable : false,
										flex : 1
									}],
							store : Ext.create("Ext.data.Store", {
										model : modelName,
										autoLoad : false,
										data : []
									}),
							listeners : {
								select : {
									fn : me.onCityGridSelect,
									scope : me
								}
							}
						});

				return me.__cityGrid;
			},

			getDistrictGrid : function() {
				var me = this;
				if (me.__districtGrid) {
					return me.__districtGrid;
				}

				var modelName = "PSIDistrict";
				Ext.define(modelName, {
							extend : "Ext.data.Model",
							fields : ["id", "name"]
						});

				me.__districtGrid = Ext.create("Ext.grid.Panel", {
							border : 0,
							viewConfig : {
								enableTextSelection : true
							},
							columnLines : true,
							columns : [{
										xtype : "rownumberer"
									}, {
										header : "区/县",
										dataIndex : "name",
										menuDisabled : true,
										sortable : false,
										flex : 1
									}],
							store : Ext.create("Ext.data.Store", {
										model : modelName,
										autoLoad : false,
										data : []
									}),
							listeners : {}
						});

				return me.__districtGrid;
			},

			freshProvinceGrid : function() {
				var me = this;

				me.getCityGrid().getStore().removeAll();
				me.getDistrictGrid().getStore().removeAll();

				var grid = me.getProvinceGrid();
				var el = grid.getEl() || Ext.getBody();
				el.mask(PSI.Const.LOADING);
				Ext.Ajax.request({
							url : PSI.Const.BASE_URL
									+ "Home/Customer/provinceList",
							method : "POST",
							callback : function(options, success, response) {
								var store = grid.getStore();

								store.removeAll();

								if (success) {
									var data = Ext.JSON
											.decode(response.responseText);
									store.add(data);
								}

								el.unmask();
							}
						});

			},

			freshCityGrid : function() {
				var me = this;

				var item = me.getProvinceGrid().getSelectionModel()
						.getSelection();
				if (item == null || item.length != 1) {
					return;
				}

				var province = item[0];

				var grid = me.getCityGrid();
				var el = grid.getEl() || Ext.getBody();
				el.mask(PSI.Const.LOADING);
				Ext.Ajax.request({
							url : PSI.Const.BASE_URL + "Home/Customer/cityList",
							params : {
								provinceId : province.get("id")
							},
							method : "POST",
							callback : function(options, success, response) {
								var store = grid.getStore();

								store.removeAll();

								if (success) {
									var data = Ext.JSON
											.decode(response.responseText);
									store.add(data);
								}

								el.unmask();
							}
						});

			},

			freshDistrictGrid : function() {
				var me = this;

				var item = me.getCityGrid().getSelectionModel().getSelection();
				if (item == null || item.length != 1) {
					return;
				}

				var city = item[0];

				var grid = me.getDistrictGrid();
				var el = grid.getEl() || Ext.getBody();
				el.mask(PSI.Const.LOADING);
				Ext.Ajax.request({
							url : PSI.Const.BASE_URL
									+ "Home/Customer/districtList",
							params : {
								cityId : city.get("id")
							},
							method : "POST",
							callback : function(options, success, response) {
								var store = grid.getStore();

								store.removeAll();

								if (success) {
									var data = Ext.JSON
											.decode(response.responseText);
									store.add(data);
								}

								el.unmask();
							}
						});
			},

			onAddProvince : function() {
				var me = this;

				var form = Ext.create("PSI.Customer.ProvinceEditForm", {
							parentForm : me
						});
				form.show();
			},

			onEditProvince : function() {
				var me = this;

				var item = me.getProvinceGrid().getSelectionModel()
						.getSelection();
				if (item == null || item.length != 1) {
					PSI.MsgBox.showInfo("请选择要编辑的省");
					return;
				}

				var province = item[0];

				var form = Ext.create("PSI.Customer.ProvinceEditForm", {
							parentForm : me,
							entity : province
						});

				form.show();
			},

			onDeleteProvince : function() {
				var me = this;

				var item = me.getProvinceGrid().getSelectionModel()
						.getSelection();
				if (item == null || item.length != 1) {
					PSI.MsgBox.showInfo("请选择要删除的省");
					return;
				}

				var province = item[0];
				var info = "请确认是否删除省 <span style='color:red'>"
						+ province.get("name") + "</span> ?";

				var funcConfirm = function() {
					var el = Ext.getBody();
					el.mask(PSI.Const.LOADING);
					var r = {
						url : PSI.Const.BASE_URL
								+ "Home/Customer/deleteProvince",
						params : {
							id : province.get("id")
						},
						method : "POST",
						callback : function(options, success, response) {
							el.unmask();
							if (success) {
								var data = Ext.JSON
										.decode(response.responseText);
								if (data.success) {
									PSI.MsgBox.tip("成功完成删除操作");
									me.freshProvinceGrid();
								} else {
									PSI.MsgBox.showInfo(data.msg);
								}
							} else {
								PSI.MsgBox.showInfo("网络错误");
							}
						}
					};

					Ext.Ajax.request(r);
				};

				PSI.MsgBox.confirm(info, funcConfirm);
			},

			onAddCity : function() {
				var me = this;

				var item = me.getProvinceGrid().getSelectionModel()
						.getSelection();
				var province;
				if (item == null || item.length != 1) {
					province = null;
				} else {
					province = item[0];
				}

				var form = Ext.create("PSI.Customer.CityEditForm", {
							parentForm : me,
							province : province
						});
				form.show();
			},

			onEditCity : function() {
				var me = this;

				var item = me.getCityGrid().getSelectionModel().getSelection();
				if (item == null || item.length != 1) {
					PSI.MsgBox.showInfo("没有选择要编辑的市");
					return;
				}

				var city = item[0];

				var item = me.getProvinceGrid().getSelectionModel()
						.getSelection();
				var province;
				if (item == null || item.length != 1) {
					province = null;
				} else {
					province = item[0];
				}

				var form = Ext.create("PSI.Customer.CityEditForm", {
							parentForm : me,
							province : province,
							entity : city
						});
				form.show();
			},

			onDeleteCity : function() {
				var me = this;

				var item = me.getCityGrid().getSelectionModel().getSelection();
				if (item == null || item.length != 1) {
					PSI.MsgBox.showInfo("没有选择要删除的市");
					return;
				}

				var city = item[0];
				var info = "请确认是否删除市 <span style='color:red'>"
						+ city.get("name") + "</span> ?";

				var funcConfirm = function() {
					var el = Ext.getBody();
					el.mask(PSI.Const.LOADING);
					var r = {
						url : PSI.Const.BASE_URL + "Home/Customer/deleteCity",
						params : {
							id : city.get("id")
						},
						method : "POST",
						callback : function(options, success, response) {
							el.unmask();
							if (success) {
								var data = Ext.JSON
										.decode(response.responseText);
								if (data.success) {
									PSI.MsgBox.tip("成功完成删除操作");
									me.freshCityGrid();
								} else {
									PSI.MsgBox.showInfo(data.msg);
								}
							} else {
								PSI.MsgBox.showInfo("网络错误");
							}
						}
					};

					Ext.Ajax.request(r);
				}

				PSI.MsgBox.confirm(info, funcConfirm);
			},

			onAddDistrict : function() {
				var me = this;

				var item = me.getCityGrid().getSelectionModel().getSelection();
				if (item == null || item.length != 1) {
					PSI.MsgBox.showInfo("请先选择市");
					return;
				}

				var city = item[0];
				var form = Ext.create("PSI.Customer.DistrictEditForm", {
							parentForm : me,
							city : city
						});
				form.show();
			},

			onEditDistrict : function() {
				var me = this;

				var item = me.getCityGrid().getSelectionModel().getSelection();
				if (item == null || item.length != 1) {
					PSI.MsgBox.showInfo("请先选择市");
					return;
				}
				var city = item[0];

				var item = me.getDistrictGrid().getSelectionModel()
						.getSelection();
				if (item == null || item.length != 1) {
					PSI.MsgBox.showInfo("请选择要编辑的区县");
					return;
				}

				var district = item[0];

				var form = Ext.create("PSI.Customer.DistrictEditForm", {
							parentForm : me,
							city : city,
							entity : district
						});
				form.show();
			},

			onDeleteDistrict : function() {
				var me = this;

				var item = me.getDistrictGrid().getSelectionModel()
						.getSelection();
				if (item == null || item.length != 1) {
					PSI.MsgBox.showInfo("请选择要删除的区县");
					return;
				}

				var district = item[0];
				var info = "请确认是否删除区县 <span style='color:red'>"
						+ district.get("name") + "</span> ?";

				var funcConfirm = function() {
					var el = Ext.getBody();
					el.mask(PSI.Const.LOADING);
					var r = {
						url : PSI.Const.BASE_URL
								+ "Home/Customer/deleteDistrict",
						params : {
							id : district.get("id")
						},
						method : "POST",
						callback : function(options, success, response) {
							el.unmask();
							if (success) {
								var data = Ext.JSON
										.decode(response.responseText);
								if (data.success) {
									PSI.MsgBox.tip("成功完成删除操作");
									me.freshDistrictGrid();
								} else {
									PSI.MsgBox.showInfo(data.msg);
								}
							} else {
								PSI.MsgBox.showInfo("网络错误");
							}
						}
					};

					Ext.Ajax.request(r);
				}

				PSI.MsgBox.confirm(info, funcConfirm);
			},

			onGenFromCustomerData : function() {
				var me = this;

				var info = "请确认是否自动从现有的客户资料中生成区域数据?";

				var funcConfirm = function() {
					var el = Ext.getBody();
					el.mask(PSI.Const.LOADING);
					var r = {
						url : PSI.Const.BASE_URL
								+ "Home/Customer/genAllDistrictData",
						params : {},
						method : "POST",
						callback : function(options, success, response) {
							el.unmask();
							if (success) {
								var data = Ext.JSON
										.decode(response.responseText);
								if (data.success) {
									me.freshProvinceGrid();
								} else {
									PSI.MsgBox.showInfo(data.msg);
								}
							} else {
								PSI.MsgBox.showInfo("网络错误");
							}
						}
					};

					Ext.Ajax.request(r);
				}

				PSI.MsgBox.confirm(info, funcConfirm);
			},

			onProvinceGridSelect : function() {
				var me = this;
				me.getCityGrid().getStore().removeAll();
				me.getDistrictGrid().getStore().removeAll();

				me.freshCityGrid();
			},

			onCityGridSelect : function() {
				var me = this;
				me.getDistrictGrid().getStore().removeAll();
				me.freshDistrictGrid();
			}
		});