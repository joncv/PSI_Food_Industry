/**
 * 市 - 新增或编辑界面
 */
Ext.define("PSI.Customer.CityEditForm", {
			extend : "Ext.window.Window",

			config : {
				parentForm : null,
				entity : null,
				province : null
			},

			/**
			 * 初始化组件
			 */
			initComponent : function() {
				var me = this;

				var entity = me.getEntity();

				me.adding = entity == null;

				var buttons = [];
				if (!entity) {
					var btn = {
						text : "保存并继续新增",
						formBind : true,
						handler : function() {
							me.onOK(true);
						},
						scope : me
					};

					buttons.push(btn);
				}

				var btn = {
					text : "保存",
					formBind : true,
					iconCls : "PSI-button-ok",
					handler : function() {
						me.onOK(false);
					},
					scope : me
				};
				buttons.push(btn);

				var btn = {
					text : entity == null ? "关闭" : "取消",
					handler : function() {
						me.close();
					},
					scope : me
				};
				buttons.push(btn);

				Ext.apply(me, {
							title : entity == null ? "新增市" : "编辑市",
							modal : true,
							resizable : false,
							onEsc : Ext.emptyFn,
							width : 400,
							height : 140,
							layout : "fit",
							listeners : {
								show : {
									fn : me.onWndShow,
									scope : me
								},
								close : {
									fn : me.onWndClose,
									scope : me
								}
							},
							items : [{
								id : "editCityForm",
								xtype : "form",
								layout : {
									type : "table",
									columns : 1
								},
								height : "100%",
								bodyPadding : 5,
								defaultType : 'textfield',
								fieldDefaults : {
									labelWidth : 40,
									labelAlign : "right",
									labelSeparator : "",
									msgTarget : 'side',
									width : 370,
									margin : "5"
								},
								items : [{
											id : "editProvinceId",
											xtype : "hidden",
											name : "provinceId"
										}, {
											xtype : "hidden",
											name : "id",
											value : entity == null
													? null
													: entity.get("id")
										}, {
											id : "editProvinceCombo",
											xtype : "combo",
											queryMode : "local",
											editable : false,
											valueField : "id",
											fieldLabel : "省",
											allowBlank : false,
											blankText : "没有输入省",
											beforeLabelTextTpl : PSI.Const.REQUIRED,
											store : Ext.create(
													"Ext.data.ArrayStore", {
														fields : ["id", "text"],
														data : []
													})
										}, {
											id : "editCityName",
											fieldLabel : "市",
											allowBlank : false,
											blankText : "没有输入市名称",
											beforeLabelTextTpl : PSI.Const.REQUIRED,
											name : "name",
											value : entity == null
													? null
													: entity.get("name"),
											listeners : {
												specialkey : {
													fn : me.onEditNameSpecialKey,
													scope : me
												}
											}
										}],
								buttons : buttons
							}]
						});

				me.callParent(arguments);
			},

			/**
			 * 保存
			 */
			onOK : function(thenAdd) {
				var me = this;

				Ext.getCmp("editProvinceId").setValue(Ext
						.getCmp("editProvinceCombo").getValue());

				var f = Ext.getCmp("editCityForm");
				var el = f.getEl();
				el.mask(PSI.Const.SAVING);
				var sf = {
					url : PSI.Const.BASE_URL + "Home/Customer/editCity",
					method : "POST",
					success : function(form, action) {
						me.__lastId = action.result.id;

						el.unmask();

						PSI.MsgBox.tip("数据保存成功");
						me.focus();
						if (thenAdd) {
							me.clearEdit();
						} else {
							me.close();
						}
					},
					failure : function(form, action) {
						el.unmask();
						PSI.MsgBox.showInfo(action.result.msg, function() {
									Ext.getCmp("editCityName").focus();
								});
					}
				};
				f.submit(sf);
			},

			onEditNameSpecialKey : function(field, e) {
				var me = this;

				if (e.getKey() == e.ENTER) {
					var f = Ext.getCmp("editCityForm");
					if (f.getForm().isValid()) {
						me.onOK(me.adding);
					}
				}
			},

			clearEdit : function() {
				Ext.getCmp("editCityName").focus();

				var editors = [Ext.getCmp("editCityName")];
				for (var i = 0; i < editors.length; i++) {
					var edit = editors[i];
					edit.setValue(null);
					edit.clearInvalid();
				}
			},

			onWndClose : function() {
				var me = this;
				if (me.__lastId) {
					me.getParentForm().freshCityGrid();
				}
			},

			onWndShow : function() {
				var me = this;

				var edit = Ext.getCmp("editCityName");
				edit.focus();
				edit.setValue(edit.getValue());

				var storeProvince = me.getParentForm().getProvinceGrid()
						.getStore();
				var comboProvince = Ext.getCmp("editProvinceCombo");
				var store = comboProvince.getStore();
				for (var i = 0; i < storeProvince.getCount(); i++) {
					var item = storeProvince.getAt(i);
					store.add({
								id : item.get("id"),
								text : item.get("name")
							});
				}

				var province = me.getProvince();
				if (province) {
					comboProvince.setValue(province.get("id"));
				}
			}
		});